(function(){

  //Creates a container div for the widget
  createDisplayDiv();

  //Init stylesheet
  initializeStyles();


  var script = document.querySelector('script[src="widget.js"]');
  var topics = script.getAttribute('data-topic');
  var totalImages = script.getAttribute('data-nbr-images');
  var tags = encodeURIComponent(topics);

 var url = `https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=b36822a18ddea0e3bd497a333e3b696d&tags=${tags}&format=json&nojsoncallback=1`;

 var flickrRquest = new XMLHttpRequest();

 flickrRquest.onreadystatechange = function() {
    if (flickrRquest.readyState == 4 && flickrRquest.status == 200) {
      var response = JSON.parse(flickrRquest.responseText);
      var photos = response.photos.photo;
      var filteredPhotos = photos.slice(0, totalImages);

      var output = '<div class="container">';
      output += '<div class="row">';

      filteredPhotos.forEach(function(photo){
          output += '<div class="photo col-lg-4 col-md-4 col-sm-4 col-xs-6">';
          output += `<img src="https://farm${photo.farm}.staticflickr.com/${photo.server}/${photo.id}_${photo.secret}.jpg" class="img-responsive">`;
          output += '</div>';
      });
      output += '</div>';
      output += '</div>';
      document.getElementById('flickr-widget').innerHTML = output;
    }
 };

 flickrRquest.open('GET', url, true);
 flickrRquest.send();

})();

function createDisplayDiv() {
  var widgetDiv = document.createElement('div');
  widgetDiv.id = "flickr-widget";
  document.getElementsByTagName('body')[0].appendChild(widgetDiv);
}

function initializeStyles() {
  document.querySelector('head').innerHTML += '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css"/>';
  document.querySelector('head').innerHTML += '<link rel="stylesheet" href="styles.css" type="text/css"/>';
}
